﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NanoMapper {

    public static class NanoMapperExtensions {

        /// <summary>
        /// Applies the source to the target
        /// </summary>
        /// <returns>The target instance</returns>
        public static TTarget ApplyTo<TTarget, TSource>(this TSource source, TTarget target, Action<MappingConfigurator<TSource>> mapper = null)
        where TSource : class
        where TTarget : class {

            // Bail out early if no mapping is required or available
            if (source == null || target == null || source == target) {
                return target;
            }

            var sourceType = typeof(TSource);
            var targetType = typeof(TTarget);

            var sourceMembers = sourceType.GetMembers(BindingFlags.GetProperty|BindingFlags.GetField);
            var targetMembers = targetType.GetMembers(BindingFlags.SetProperty|BindingFlags.SetField);

            var members = from t in targetMembers
                          from s in sourceMembers
                          where t.Name == s.Name && t.ReflectedType.IsAssignableFrom(s.ReflectedType)
                          select new {
                              SourceMember = s,
                              TargetMember = t
                          };

            foreach (var member in members) {
                var value = GetMemberValue(member.SourceMember, source);

                if (value == null) {
                    // TODO: Null value handling
                }

                SetMemberValue(member.TargetMember, target, value);
            }

            return target;

            /// <summary>
            /// Gets the member's value.
            /// </summary>
            object GetMemberValue(MemberInfo member, object obj) {
                if (member is PropertyInfo property) {
                    return property.GetValue(obj);
                }

                if (member is FieldInfo field) {
                    return field.GetValue(obj);
                }

                throw new InvalidProgramException($"INTERNAL: Cannot use member type mapping '{member.MemberType}'");
            }

            /// <summary>
            /// Sets the member's value.
            /// </summary>
            void SetMemberValue(MemberInfo member, object obj, object value) {
                if (member is PropertyInfo property) {
                    property.SetValue(obj, value);
                    return;
                }

                if (member is FieldInfo field) {
                    field.SetValue(obj, value);
                    return;
                }

                throw new InvalidProgramException($"INTERNAL: Cannot use member type mapping '{member.MemberType}'");
            }
        }

        /// <summary>
        /// Applies the source to the target
        /// </summary>
        /// <returns>The source instance</returns>
        public static TSource Apply<TTarget, TSource>(this TTarget target, TSource source, Action<MappingConfigurator<TSource>> mapper = null)
        where TSource : class
        where TTarget : class {
            source.ApplyTo(target, mapper);
            return source;
        }

        /// <summary>
        /// Copies the source to a new target of the same type
        /// </summary>
        /// <returns>The new target</returns>
        public static TSource Copy<TSource>(this TSource source, Action<MappingConfigurator<TSource>> mapper = null)
        where TSource : class, new() {
            return Copy<TSource, TSource>(source, mapper);
        }

        /// <summary>
        /// Copies the source to a new target of a different but compatible type
        /// </summary>
        /// <returns>The new target</returns>
        public static TTarget Copy<TTarget, TSource>(this TSource source, Action<MappingConfigurator<TSource>> mapper = null)
        where TSource : class
        where TTarget : class, new() {
            var target = Activator.CreateInstance<TTarget>();

            source.ApplyTo(target, mapper);

            return target;
        }

    }
}
