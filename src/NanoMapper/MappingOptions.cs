﻿using System;
namespace NanoMapper {

    /// <summary>
    /// Options used to configure the mapping behaviour
    /// </summary>
    public class MappingOptions {

        /// <summary>
        /// When true, makes the mapping recursive (deep)
        /// </summary>
        public bool? Recursive { get; set; }

        /// <summary>
        /// When true, only properties explicitly defined on the class will be mapped.
        /// Useful for excluding properties from base classes and inheritence trees.
        /// </summary>
        public bool? IgnoreHeritage { get; set; }

        /// <summary>
        /// Determines how null values are handled
        /// </summary>
        public NullValueHandler NullValueHandler { get; set; }

        /// <summary>
        /// Mapping option defaults
        /// </summary>
        public static readonly MappingOptions Defaults = new MappingOptions {
            Recursive = false,
            IgnoreHeritage = false,
            NullValueHandler = new ApplyNullValues()
        };

        /// <summary>
        /// Creates a new MappingOptions instance by copying the global defaults.
        /// Optionally overrideing specific options
        /// </summary>
        public static MappingOptions CreateFromDefaults(MappingOptions overrides = null) {
            return Defaults.Copy().Apply(overrides);
        }
    }

    public abstract class NullValueHandler {

    }

    /// <summary>
    /// Applies null values.
    /// </summary>
    public class ApplyNullValues : NullValueHandler { }

    /// <summary>
    /// Ignores null values.
    /// </summary>
    public class IgnoreNullValues : NullValueHandler { }

    /// <summary>
    /// Overrides null values with a user defined replacement
    /// </summary>
    public class ReplaceNullValues : NullValueHandler { }

}
