﻿using System;
using System.Linq.Expressions;

namespace NanoMapper {

    /// <summary>
    /// Configures the mappig execution context.
    /// </summary>
    public class MappingConfigurator<TSource> {

        /// <summary>
        /// Configurable mapping options 
        /// </summary>
        public MappingOptions Options { get; } = MappingOptions.CreateFromDefaults();

        /// <summary>
        /// Ignore the specified property.
        /// </summary>
        public void Ignore<TProperty>(Expression<Func<TSource, TProperty>> propertyExpression) {

        }

        /// <summary>
        /// Configures a mapping function for the specified property.
        /// </summary>
        public void Map<TProperty>(Expression<Func<TSource, TProperty>> propertyExpression, Func<TSource, TProperty> mappingFunction) {

        }

        /// <summary>
        /// Configures a mapping function for type mappings.
        /// </summary>
        public void Map<TTarget>(Func<TSource, TTarget> mappingFunction) {

        }
    }

}
