﻿using System;
namespace NanoMapper {

    public class MappingException : Exception {
        public MappingException(string message) : base(message) { }
    }

}
