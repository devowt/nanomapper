using Xunit;
using NanoMapper;

namespace NanoMapper.Tests {

    public class NanoMapperTests {

        [Fact]
        public void CanApplyFromObject() {
            var source = new {
                Name = "NanoMappper"
            };

            var target = new {
                Name = ""
            };

            target.Apply(source);

            Assert.Equal(source.Name, target.Name);
        }

        [Fact]
        public void CanApplyToObject() {
            var source = new {
                Name = "NanoMappper"
            };

            var target = new {
                Name = ""
            };

            source.ApplyTo(target);

            Assert.Equal(source.Name, target.Name);
        }

        [Fact]
        public void CanCopyObject() {
            var source = new SimpleClass {
                Name = "NanoMappper"
            };

            var target = source.Copy();

            Assert.Equal(source.Name, target.Name);
        }

        [Fact]
        public void CanIgnoreProperties() {
            var source = new SimpleClass {
                Name = "NanoMappper",
                Description = "This will be ignored"
            };

            var target = source.Copy(mapper => {
                mapper.Ignore(s => s.Description);
            });

            Assert.Equal(source.Name, target.Name);
            Assert.Null(target.Description);
        }

        [Fact]
        public void CanOverrideDefaultPropertyMapping() {
            var source = new SimpleClass {
                Name = "NanoMappper",
                Description = "This will be augmented"
            };

            var target = source.Copy(mapper => {
                mapper.Map(s => s.Description, s => {
                    return $"Changed: {s.Description}";
                });
            });

            Assert.Equal(source.Name, target.Name);
            Assert.Equal("Changed: This will be augmented", target.Description);
        }


        [Fact]
        public void CanOverrideDefaultObjectMapping() {
            var source = new SimpleClass {
                Name = "NanoMappper",
                Description = "This will be customised"
            };

            var target = source.Copy(mapper => {
                mapper.Map(s => {
                    return new {
                        Name = $"Custom: {s.Name}",
                        Description = $"Custom: {s.Description}"
                    };
                });
            });

            Assert.Equal("Custom: NanoMapper", target.Name);
            Assert.Equal("Custom: This will be customised", target.Description);
        }

        [Fact]
        public void CreatedDefaultMappingOptionsMustAlwaysBeUniqueInstances() {
            Assert.NotSame(MappingOptions.CreateFromDefaults(), MappingOptions.CreateFromDefaults());
        }


        /// <summary>
        /// Used for testing functions that only handle reference types
        /// </summary>
        public class SimpleClass {
            public string Name { get; set; }
            public string Description { get; set; }
        }
    }

}
